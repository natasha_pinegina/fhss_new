﻿// Draw_FN.cpp: файл реализации
//

#include "pch.h"
#include "LotsOfSignals.h"
#include "Draw_FN.h"
#include "afxdialogex.h"
#include "LotsOfSignalsDlg.h"
#include <GL/gl.h>
#include <GL/glu.h>


// Диалоговое окно Draw_FN

IMPLEMENT_DYNAMIC(Draw_FN, CDialogEx)

Draw_FN::Draw_FN(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_FN, pParent)
{

}

Draw_FN::~Draw_FN()
{
}

void Draw_FN::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OX, OX);
	DDX_Control(pDX, IDC_OY, OY);
	DDX_Control(pDX, IDC_OZ, OZ);
	DDX_Control(pDX, IDC_zoom, zoom);
	DDX_Control(pDX, IDC_Osi, Osi);
	DDX_Control(pDX, IDC_Linii, Linii);
}


BEGIN_MESSAGE_MAP(Draw_FN, CDialogEx)
	ON_BN_CLICKED(IDC_DrawFN, &Draw_FN::OnBnClickedDrawfn)
END_MESSAGE_MAP()

BOOL Draw_FN::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	pDC_new = GetDlgItem(IDC_FN)->GetDC();
	InitiateOPGL();

	OX.SetRangeMin(-180, TRUE);			// минимум
	OX.SetRangeMax(180, TRUE);
	OX.SetPos(-30);
	OY.SetRangeMin(-180, TRUE);			// минимум
	OY.SetRangeMax(180, TRUE);
	OY.SetPos(-10);
	OZ.SetRangeMin(-180, TRUE);			// минимум
	OZ.SetRangeMax(180, TRUE);
	OZ.SetPos(0);

	zoom.SetRangeMin(0, TRUE);			// минимум
	zoom.SetRangeMax(1000, TRUE);
	zoom.SetPos(250);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // Исключение: страница свойств OCX должна возвращать значение FALSE
}

// Обработчики сообщений Draw_FN

void Draw_FN::find_max_min(vector<vector<double>>& D, double& max, double& min, double& jmax, double& imax)
{
	max = D[0][0];
	min = D[0][0];
	for (int j = 1; j < D.size() - 1; j++)
	{
		for (int i = 1; i < D[0].size() - 1; i++)
		{

			if (max < D[j][i])
			{
				max = D[j][i];
				jmax = j;
				imax = i;
			}
			if (min > D[j][i])
			{
				min = D[j][i];
			}
		}
	}
}

BOOL Draw_FN::bSetupPixelFormat()
{
	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),  // size of this pfd
		1,                              // version number
		PFD_DRAW_TO_WINDOW |            // support window
		  PFD_SUPPORT_OPENGL |          // support OpenGL
		  PFD_DOUBLEBUFFER,             // double buffered
		PFD_TYPE_RGBA,                  // RGBA type
		24,                             // 24-bit color depth
		0, 0, 0, 0, 0, 0,               // color bits ignored
		0,                              // no alpha buffer
		0,                              // shift bit ignored
		0,                              // no accumulation buffer
		0, 0, 0, 0,                     // accum bits ignored
		32,                             // 32-bit z-buffer
		0,                              // no stencil buffer
		0,                              // no auxiliary buffer
		PFD_MAIN_PLANE,                 // main layer
		0,                              // reserved
		0, 0, 0                         // layer masks ignored
	};
	int pixelformat;
	if ((pixelformat = ChoosePixelFormat(pDC_new->GetSafeHdc(), &pfd)) == 0)
	{
		MessageBox(L"ChoosePixelFormat failed");
		return FALSE;
	}

	if (SetPixelFormat(pDC_new->GetSafeHdc(), pixelformat, &pfd) == FALSE)
	{
		MessageBox(L"SetPixelFormat failed");
		return FALSE;
	}
	return TRUE;
}

void Draw_FN::InitiateOPGL()
{
	CRect rect, rect1;
	HGLRC hrc;
	if (!bSetupPixelFormat())
	{
		return;
	}
	hrc = wglCreateContext(pDC_new->GetSafeHdc());
	ASSERT(hrc != NULL);
	wglMakeCurrent(pDC_new->GetSafeHdc(), hrc);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1, 1, -1, 1, -1, 1);
	gluLookAt(0, 0, -1, 0, 0, 1, 0, 100, 0);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	//gluPerspective(15.0f, (GLfloat)rect.right / (GLfloat)rect.bottom, 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);

}

double Draw_FN::findmax(vector<vector<double>>& D, bool max_min)
{
	double find = D[0][0];

	if (max_min)
	{
		for (int i = 0; i < D.size(); i++)
		{
			for (int j = 0; j < D[i].size(); j++)
			{
				if (D[i][j] > find)
				{
					find = D[i][j];
				}
			}
		}
	}
	else
	{
		for (int i = 0; i < D.size(); i++)
		{
			for (int j = 0; j < D[i].size(); j++)
			{
				if (D[i][j] < find)
				{
					find = D[i][j];
				}
			}
		}
	}
	return find;
}

void Draw_FN::DrawGL(vector<vector<double>>& WV)
{

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	gluLookAt(0, 0, 0, 0, 0, 1, 0, 1, 0);
	glRotated((double)OX.GetPos(), 1.0f, 0.0f, 0.0f);
	glRotated((double)OY.GetPos(), 0.0f, 1.0f, 0.0f);
	glRotated((double)OZ.GetPos(), 0.0f, 0.0f, 1.0f);
	double mash = (double)zoom.GetPos() / 200;
	if (firsttimedraw)//1
	{
		max = findmax(WV, 1);
		min = findmax(WV, 0);
		if (abs(max) > abs(min))
		{
			absmax = abs(max);
		}
		else
		{
			absmax = abs(min);
		}
		maxmin = abs(max) + abs(min);
		firsttimedraw = 0;
	}
	glPointSize(1);

	for (int i = 1; i < WV.size() - 2; i++)
	{
		for (int j = 1; j < WV[i].size() - 1; j++)
		{
			glBegin(GL_TRIANGLE_STRIP);

			glColor3d((WV[i][j] - min) / maxmin * 100, 0.0f, 1 - (WV[i][j] - min) / maxmin);
			glVertex3d((double)(j - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i][j] / 2 / absmax / mash);

			glColor3d((WV[i + 1][j] - min) / maxmin * 100, 0.0f, 1 - (WV[i + 1][j] - min) / maxmin);
			glVertex3d((double)(j - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i + 1 - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i + 1][j] / 2 / absmax / mash);

			glColor3d((WV[i][j + 1] - min) / maxmin * 100, 0.0f, 1 - (WV[i][j + 1] - min) / maxmin);
			glVertex3d((double)(j + 1 - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i][j + 1] / 2 / absmax / mash);

			glColor3d((WV[i + 1][j] - min) / maxmin * 100, 0.0f, 1 - (WV[i + 1][j] - min) / maxmin);
			glVertex3d((double)(j - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i + 1 - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i + 1][j] / 2 / absmax / mash);

			glColor3d((WV[i][j + 1] - min) / maxmin * 100, 0.0f, 1 - (WV[i][j + 1] - min) / maxmin);
			glVertex3d((double)(j + 1 - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i][j + 1] / 2 / absmax / mash);

			glColor3d((WV[i + 1][j + 1] - min) / maxmin * 100, 0.0f, 1 - (WV[i + 1][j + 1] - min) / maxmin);
			glVertex3d((double)(j + 1 - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i + 1 - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i + 1][j + 1] / 2 / absmax / mash);

			glEnd();
		}
	}

	if (Osi.GetCheck() == BST_CHECKED)
	{
		int i = WV.size() / 2;
		int j = WV[0].size() / 2;

		glLineWidth(9);
		glBegin(GL_LINE_STRIP);
		glColor3d(1.0f, 0.0f, 0.0f);
		glVertex3d((double)(-(double)WV.size() / 2) * 2 / (double)WV.size() / mash, (double)(-(double)WV[0].size() / 2)* 2 / (double)WV[0].size() / mash, min);

		glColor3d(1.0f, 0.0f, 0.0f);
		glVertex3d((double)(-(double)WV.size() / 2) * 2 / (double)WV.size() / mash, (double)((double)WV[0].size() / 2)* 2 / (double)WV[0].size() / mash, min);

		glColor3d(0.0f, 1.0f, 0.0f);
		glVertex3d((double)(-(double)WV.size() / 2) * 2 / (double)WV.size() / mash, (double)(-(double)WV[0].size() / 2)* 2 / (double)WV[0].size() / mash, min);

		glColor3d(0.0f, 1.0f, 0.0f);
		glVertex3d((double)(WV.size() - (double)WV.size() / 2) * 2 / (double)WV.size() / mash, (double)(-(double)WV[0].size() / 2)* 2 / (double)WV[0].size() / mash, min);

		/*glColor3d(0.0f, 0.0f, 1.0f);
		glVertex3d(i, j, min);

		glColor3d(0.0f, 0.0f, 1.0f);
		glVertex3d(i, j, max);*/

		glEnd();
	}


	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	if(Linii.GetCheck()== BST_CHECKED)
	
	{
		glLineWidth(3);
		glBegin(GL_QUADS);
		for (int i = steplines; i < WV.size() - steplines; i += steplines)
		{
			for (int j = steplines; j < WV[i].size() - steplines; j += steplines)
			{
				//for (int k = 0; k < del.size(); k++)
				//{
					/*glColor3d(1.0f, 1.0f, 1.0f);
					if (abs(i - del[k][1]) < 10 && abs(j - del[k][0]) < 10)
					{*/
					glColor3d(1.0f, 1.0f, 1.0f);

					glVertex3d((double)(j - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i][j] / 2 / absmax / mash + 1. / 50 / mash);
					glVertex3d((double)(j - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i + steplines - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i + steplines][j] / 2 / absmax / mash + 1. / 50 / mash);
					glVertex3d((double)(j + steplines - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i + steplines - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i + steplines][j + steplines] / 2 / absmax / mash + 1. / 50 / mash);
					glVertex3d((double)(j + steplines - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i][j + steplines] / 2 / absmax / mash + 1. / 50 / mash);
					//}	

				//}
			}
		}
		glEnd();

	}
	glFinish();
	SwapBuffers(wglGetCurrentDC());

}

void Draw_FN::OnBnClickedDrawfn()
{
	CLotsOfSignalsDlg* pParent = (CLotsOfSignalsDlg*)GetParent();
	
	
	vector<vector<double>> buf;
	
	pParent->OtrisovkaFN(buf);
	firsttimedraw = 1;
	DrawGL(buf);
	buf.clear();
}



