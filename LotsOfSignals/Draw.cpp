#include "pch.h"
#include "LotsOfSignalsDlg.h"

#define KOORD(x,y) (xp*((x)-xmin)),(yp*((y)-ymax)) 

void CLotsOfSignalsDlg::Pererisovka(CDC* WinDc, CRect WinPic, CPen* graphpen, double MinX, double MaxX, double MinY, double MaxY)
{
	xmin = MinX;			//����������� �������� �
	xmax = MaxX;			//������������ �������� �
	ymin = MinY;			//����������� �������� y
	ymax = MaxY;			//������������ �������� y


	xp = ((double)(WinPic.Width()) / (xmax - xmin));			//������������ ��������� ��������� �� �
	yp = -((double)(WinPic.Height()) / (ymax - ymin));

	WinDc->FillSolidRect(&WinPic, RGB(250, 250, 250));			//���������� ��� 

	WinDc->SelectObject(&koordpen);
	//������ ��� Y
	WinDc->MoveTo(KOORD(0, ymax));
	WinDc->LineTo(KOORD(0, ymin));
	//������ ��� �
	WinDc->MoveTo(KOORD(xmin, 0));
	WinDc->LineTo(KOORD(xmax, 0));

	//������� ����
	WinDc->TextOutW(KOORD(0, ymax - 0.2), _T("Y")); //Y
	WinDc->TextOutW(KOORD(xmax - 0.3, 0 - 0.2), _T("t")); //X

	WinDc->SelectObject(&setka_pen);

	//��������� ����� �� y
	for (float x = xmin ; x <= xmax; x += xmax / 10)
	{
		WinDc->MoveTo(KOORD(x, ymax));
		WinDc->LineTo(KOORD(x, ymin));
	}
	//��������� ����� �� x
	for (float y = ymin ; y <= ymax; y += ymax / 5)
	{
		WinDc->MoveTo(KOORD(xmin, y));
		WinDc->LineTo(KOORD(xmax, y));
	}


	//������� ����� �� ���
	CFont font3;
	font3.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	WinDc->SelectObject(font3);
	//�� Y � ����� 5
	for (double i = ymin; i <= ymax; i += ymax / 5)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		WinDc->TextOutW(KOORD(0.2, i), str);
	}
	//�� X � ����� 0.5
	for (double j = xmin; j <= xmax; j += xmax / 10)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		WinDc->TextOutW(KOORD(j, -0.03), str);
	}
}