#pragma once
#include "afxdialogex.h"


// ���������� ���� CDrawGraph

class CDrawGraph : public CDialog
{
	DECLARE_DYNAMIC(CDrawGraph)

public:
	CDrawGraph(CWnd* pParent = nullptr);   // ����������� �����������
	virtual ~CDrawGraph();

	// ������ ����������� ����
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DRAW_GRAPH };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

	DECLARE_MESSAGE_MAP()
};