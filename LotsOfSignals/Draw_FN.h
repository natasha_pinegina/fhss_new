﻿#pragma once

//#include "LotsOfSignalsDlg.h"
#include <vector>

using namespace std;

// Диалоговое окно Draw_FN

class Draw_FN : public CDialogEx
{
	DECLARE_DYNAMIC(Draw_FN)

public:
	Draw_FN(CWnd* pParent = nullptr);   // стандартный конструктор
	virtual ~Draw_FN();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FN };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedDrawfn();
	CDC* pDC_new;
	virtual BOOL OnInitDialog();
	void find_max_min(vector<vector<double>>& D, double& max, double& min, double& jmax, double& jmin);
	void InitiateOPGL();
	BOOL bSetupPixelFormat();
	void DrawGL(vector<vector<double>>& WV);
	//bool lines=true;
	bool lines = false;
	int steplines = 5;
	int dimx = 150;
	int dimy = 150;
	double max, min;
	double absmax;
	bool firsttimedraw;
	double maxmin;
	double findmax(vector<vector<double>>& D, bool max_min);
	CSliderCtrl OX;
	CSliderCtrl OY;
	CSliderCtrl OZ;
	CSliderCtrl zoom;
	CButton Osi;
	CButton Linii;
};
