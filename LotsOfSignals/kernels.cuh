#pragma once
#ifdef __CUDACC__
#define __CUDACC__
#endif 
#ifdef __CUDACC_RTC__
#define __CUDACC_RTC__
#endif 
#include <cuda.h>
#include <device_functions.h>
#include <device_launch_parameters.h>
#include <cuda_runtime_api.h>
#include <cuda_texture_types.h>

/********************* ���������� ���� CUDA ****************************/


extern __global__ void mapping(int* map, int width, int height);

extern __global__ void decimation(float2* ref_signal,
	float2* obs_signal,
	float2* matrix_dec,
	int decimation_step);

extern __global__ void matMult(float2* ref_signal,
	float2* obs_signal,
	int decimation,
	int tile_dim,
	float2* out_matrix);

extern __global__ void transpose(float2* matrix_in,
	float2* matrix_out,
	int width,
	int height);

extern __global__ void IIR_filter(float2* spectrum,
	int sample1,
	int sample2);
