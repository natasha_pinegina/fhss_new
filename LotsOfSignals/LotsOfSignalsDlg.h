﻿
// LotsOfSignalsDlg.h: файл заголовка
//

#pragma once
#pragma comment(linker, "/HEAP:2000000")

#include <vector>
#include <complex>
#include "Generation.h"
#include "afxcmn.h"
#include "resource.h"
#include "Draw_FN.h"

using namespace std;

// Диалоговое окно CLotsOfSignalsDlg
class CLotsOfSignalsDlg : public CDialogEx
{
// Создание
public:
	CLotsOfSignalsDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LOTSOFSIGNALS_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV

	CWnd* PicWnd_Input_Sign;
	CDC* PicDc_Input_Sign;
	CRect Pic_Input_Sign;

	CWnd* PicWnd_AKP;
	CDC* PicDc_AKP;
	CRect Pic_AKP;

	CWnd* PicWnd_Cor2;
	CDC* PicDc_Cor2;
	CRect Pic_Cor2;

	CWnd* PicWnd_Cor1;
	CDC* PicDc_Cor1;
	CRect Pic_Cor1;

	CWnd* PicWnd_MAX;
	CDC* PicDc_MAX;
	CRect Pic_MAX;

	CPen koordpen, netkoordpen, signalpen, spectrpen, vsignalpen;
	CPen* pen;
	CFont fontgraph;
	CFont* font;

	CPen osi_pen;		// для осей 
	CPen setka_pen;		// для сетки
	CPen graf_pen;		// для графика функции
	CPen graf_pen2;
	CPen graf_pen3;

	double
		xx0,
		xxmax,
		yy0,
		yymax,
		xxi,
		yyi,
		iter;

	char
		znach[1000];

	double
		Max,
		Min,
		Mass1Min,
		Mass1Max,
		Mass2Min,
		Mass2Max;

	double xp, yp,			//коэфициенты пересчета
		xmin, xmax,			//максисимальное и минимальное значение х 
		ymin, ymax;			//максисимальное и минимальное значение y


	double xminget, xmaxget,
		yminget, ymaxget,
		xpget, ypget;

	void Draw1Graph(std::vector<double>&, CDC*, CRect, CPen*, int, int, CString, CString);
	/*void Draw2Graph(float*, CPen*, float*, CPen*, CDC*, CRect, float, CString, CString);*/
	void Draw2Graph(std::vector<double>&, CPen*, std::vector<double>&, CPen*, CDC*, CRect, float, CString, CString);
	void DrawGraph(std::vector<std::vector<double>>& Mass, double MinX, double MaxX, std::vector<CPen*> GraphPen, CDC* WinDc, CRect WinPic);
	void DrawKoord(CDC*, CRect, CString, CString);
	void PererisovkaGetData(CDC* WinDc, CRect WinPic, CPen* graphpen, double MinX, double MaxX, double MinY, double MaxY);
	void krug(double x, double y, double r, CDC* WinDc);
	//void Draw1Graph(std::vector<complex<double>>, CDC*, CRect, CPen*, int, int, CString, CString);

// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	afx_msg double signal(double t);
	afx_msg  void correlate(std::vector<double>& signal1, std::vector<double>& signal2, vector<complex<double>> correlation, std::vector<double>& corr);


	double N=256;
	
public:
	double A1;
	double A2;
	double A3;
	double F1;
	double F2;
	double F3;
	double P1;
	double P2;
	double P3;
	double deltaT;
	afx_msg void OnBnClickedPusk();
	afx_msg void OnBnClickedPusk2();
	CButton bpsk;
	CButton msk;
	afx_msg void GenerateTwoLongSignal();
	CDC* pDC;
	

	//частота дискретизации
	//double samplingFrequency = 300e6,
	//	//метка времени начала
	//	startTimestamp = 0,
	//	//продолжительность
	//	Duration = 1e-3,
	//	//начальная фаза
	//	startPhase = 0,
	//	nSamples = 0,
	//	//скорость передачи данных
	//	Bitrate = 100e5,
	//	//дополнительный параметр
	//	additionalParameter = 0;
	//частота дискретизации
	double samplingFrequency = 300e6;
	//метка времени начала
	double startTimestamp = 0;
	//продолжительность
	double Duration = 1e-3;
	//начальная фаза
	double startPhase = 0;
	double nSamples = 0;
	//скорость передачи данных
	double Bitrate = 1e6;
	//дополнительный параметр, нечто, напоминающее modFreq
	double additionalParameter = 0;

	CButton Sputnic2;
	CButton Sputnic3;
	CButton Correlator;
	CButton Shema;
	double T1;
	double T2;
	afx_msg void OnBnClickedEstimation();
	std::vector<double> sIgnal2;
	std::vector<double> sIgnal3;
	std::vector<double> SSignal;

	//int NumberOfSources = 6;//количество источников
	//int NumberOfSatellites = 3;//количество спутников

	vector<int> find_max_n(int n, Signal& sig, double clearance);
	vector<double> criteria(vector<vector<int>> delays, double clear);
	void Pererisovka(CDC* WinDc, CRect WinPic, CPen* graphpen, double MinX, double MaxX, double MinY, double MaxY);
	double clearance;
	vector<double> Delays(int NumberOfSources, int NumberOfSatellites);
	vector<string> str1;
	vector<string> str2;
	vector<string> str3;

	vector<string> str;
	vector<string> strr;
	vector<double> CLotsOfSignalsDlg::criteria(vector<vector<int>> delays);
	double fr;
	int NumberOfSatellites;
	int NumberOfSources;
	double shum;
	void ProbabilitiesResearch();
	void Research();
	double Shum;
	int KolOtch;
	afx_msg void OnBnClickedPusk3();
	double KolMax;
	void find_max_min(vector<vector<double>>& D, double& max, double& min, double& jmax, double& jmin);
	void InitiateOPGL();
	BOOL bSetupPixelFormat();
	void DrawGL(vector<vector<double>> WV);
	//bool lines=true;
	bool lines = false;
	int steplines=2;
	int dimx=150;
	int dimy=150;
	double max, min;
	double absmax;
	bool firsttimedraw;
	double maxmin;
	CSliderCtrl spin_x;
	CSliderCtrl spin_y;
	CSliderCtrl spin_z;
	CSliderCtrl zoom;
	double findmax(vector<vector<double>>& D, bool max_min);
	//vector<vector<double>> outsignal2D;
	HANDLE hThrds;
	HANDLE readytoexit;
	HANDLE	waittoexit;
	HANDLE	onlyrun;
	HANDLE	waitforlines;
	HANDLE	waitfordraw;
	HANDLE	waitforgennext;
	HANDLE	waitforgenstart;
	double timet;
	double fd=10;
	double dt=0.01;
	DWORD dwThread;
	HANDLE hThread;
	afx_msg void OnBnClickedPusk4();
	bool killtimer;
	int activated;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	vector<double> DelaysFrequency(int NumberOfSources, int NumberOfSatellites);
	double DoubleRand(double _max, double _min);
	void cutFrequency(Signal2D& insignal/*, Signal2D& outsignal*/);
	//Signal2D outsignal;
	vector<int> find_max_time(int n, vector<double>& sig, vector<double>& keysX,
		vector<double>& keysY, bool time_freq, double clearance);
	struct DELAYS
	{
		int time;
		int freq;
		double value;
	};
	vector<vector<int>> find_max_TF(int n, Signal2D& sig, double clearance);
	vector<double> timett;
	vector<double>freq;
	vector<vector<int>> del;
	int NumberMax;

	vector<vector<int>> Time;
	vector<vector<int>> Freq;
	int sput;
	double scale;
	vector<vector<int>> ind;
	vector<string> Stroka1;
	vector<string> Stroka2;
	CListBox LBox;
	afx_msg void OnBnClickedotrisovat();
	vector<Signal>* CorrelateSignal = new vector<Signal>(NumberOfSatellites);
	vector<Signal2D>* outsignal = new vector<Signal2D>(NumberOfSatellites);
	//CString Info;
	CString Info;
	CString sstr;
	void ReadDataFromFile(vector<double>& koef, char* way);
	void ConvolutionHS(Signal& signal, Signal& convolution);

	void ExtractionOfNarrowbandSignal(vector<Signal>& signal, vector<vector<Signal>>& conv);
	void SummA(vector<Signal2D>& A, Signal2D& newA);
	//afx_msg void OnBnClickedAlgoritm();
	vector<vector<double>> spectr_long_signal;
	vector<Signal>* SummSignal = new vector<Signal>(NumberOfSatellites);
	std::vector<CPen*> GraphPen;
	vector<vector<double>> spectr_summ_signal;
	afx_msg void OnBnClickedSvertkasfiltrom();
	afx_msg void OnBnClickedalgoritm2();
	vector<vector<Signal>>* conv = new vector<vector<Signal>>(SummSignal->size());
	vector<vector<double>> massdoubleSignal;
	void ExponentMultiplication(Signal& signal, double sdvig, double direction);
	vector<vector<double>> spectr_conv_signal;
	vector<vector<double>> BigMassOtrisovka;
	vector<vector<double>> BigMassOtshetX;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedDrawfn();

	Draw_FN* pGraphDialog = nullptr;
	vector<vector<double>> UncertaintyFunction;
	void OtrisovkaFN(vector<vector<double>>& buf);

//	virtual BOOL Create(LPCTSTR lpszTemplateName, CWnd* pParentWnd = NULL);
};


