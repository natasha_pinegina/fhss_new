#pragma once
#include "kernels.cuh"
#include <math.h>
#define BLOCK_SIZE 16

/*************************  ���������� CUDA ���� **************************/
__global__ void mapping(int* map, int width, int height)
{
	int indX = blockIdx.x * blockDim.x + threadIdx.x;
	int indY = blockIdx.y * blockDim.y + threadIdx.y;

	map[indY * width + indX] = indX * height + indX + indY;
}

/** ���� ������������� ������������ �������� � ����������  */
__global__ void decimation(float2* ref_signal,
	float2* obs_signal,
	float2* matrix_dec,
	int decimation_step)
{
	float2 mul_elem;
	mul_elem.x = 0.0f;
	mul_elem.y = 0.0f;

	// Current delay in observed signal
	int delay = blockIdx.y * blockDim.y + threadIdx.y;
	// Index in X-dimension in signals
	int indX = blockIdx.x * blockDim.x + threadIdx.x;
	// Index in Y-dimension
	int indY = blockIdx.y * blockDim.y + threadIdx.y;
	// Linear index in matrix
	int matrixIndex = indY * blockDim.x * gridDim.x + indX;
	// Index in signal sequence
	int signalIndex = decimation_step * indX;

	float2 ref_sample, obs_sample;
	ref_sample.x = 0.0f;
	ref_sample.y = 0.0f;

	obs_sample.x = 0.0f;
	obs_sample.y = 0.0f;
	// Decimation loop
	for (int i = 0; i < decimation_step; i++)
	{
		ref_sample = ref_signal[signalIndex + i];
		obs_sample = obs_signal[signalIndex + i + delay];
		// calculate sum, values of observed signal are complex conjugate
		mul_elem.x += ref_sample.x * obs_sample.x +
			ref_sample.y * obs_sample.y;
		mul_elem.y += ref_sample.y * obs_sample.x -
			ref_sample.x * obs_sample.y;
	}

	matrix_dec[matrixIndex] = mul_elem;
}

// Parallel multiplication of two signals
__global__ void matMult(float2* ref_signal,
	float2* obs_signal,
	int decimation,
	int tile_dim,
	float2* out_matrix)
{
	// Get number of current block in the grid in X and Y dimensions
	int bx = blockIdx.x;
	int by = blockIdx.y;

	// Get number of current thread in the current block in X and Y dimensions
	int tx = threadIdx.x;
	int ty = threadIdx.y;

	// Index of begin in the observed signal
	int obs_begin_index = by * decimation * BLOCK_SIZE + bx * decimation;
	// Index of the end in the observed signal
	int obs_end_index = obs_begin_index + decimation;
	// Index of begin in the reference signal
	int ref_begin_index = bx * decimation;

	// Element of the out matrix
	float2 sum;
	sum.x = 0.0f;
	sum.y = 0.0f;

	// "Slices" of the signals to write in the shared memory
	__shared__ float ref_s_real[BLOCK_SIZE];
	__shared__ float ref_s_imag[BLOCK_SIZE];
	// Add an additional column to avoid shared memory bank conflicts
	__shared__ float obs_s_real[BLOCK_SIZE][BLOCK_SIZE + 1];
	__shared__ float obs_s_imag[BLOCK_SIZE][BLOCK_SIZE + 1];


#pragma unroll //unroll the folowing loop

	// Loop for slices of signals
	for (int i_obs = obs_begin_index, i_ref = ref_begin_index;
		i_obs < obs_end_index;
		i_obs += BLOCK_SIZE, i_ref += BLOCK_SIZE)
	{
		// Store samples of the signals in shared memory
		obs_s_real[tx][ty] = obs_signal[i_obs + decimation * ty + tx].x;
		obs_s_imag[tx][ty] = obs_signal[i_obs + decimation * ty + tx].y;
		ref_s_real[tx] = ref_signal[i_ref + tx].x;
		ref_s_imag[tx] = ref_signal[i_ref + tx].y;

		// Wait for all the threads in this block (Barrier synchronization)
		__syncthreads();

		// Multiplying the samples in shared memory
		for (int k = 0; k < BLOCK_SIZE; k++)
		{
			sum.x += obs_s_real[k][tx] * ref_s_real[k] + obs_s_imag[k][tx] * ref_s_imag[k];
			sum.y += obs_s_real[k][tx] * ref_s_imag[k] - obs_s_imag[k][tx] * ref_s_real[k];
		}

		// Wait for all the threads
		__syncthreads();
	}

	// Index of out matrix to store
	int ic = bx * tile_dim + by * BLOCK_SIZE + tx;
	// Store the element in global memory
	if (ty == 0) // this condition leads to branching, 
		// code without this condition works, but 
		// it is not quite correct to not use this condition due to the "Race condition"
	{
		out_matrix[ic].x = sum.x;
		out_matrix[ic].y = sum.y;
	}
}

__global__ void transpose(float2* matrix_in,
	float2* matrix_out,
	int width,
	int height)
{
	__shared__ float tile_real[BLOCK_SIZE][BLOCK_SIZE + 1];
	__shared__ float tile_imag[BLOCK_SIZE][BLOCK_SIZE + 1];

	int xIndex = blockIdx.x * BLOCK_SIZE + threadIdx.x;
	int yIndex = blockIdx.y * BLOCK_SIZE + threadIdx.y;
	int inIndex = yIndex * width + xIndex;

	xIndex = blockIdx.y * BLOCK_SIZE + threadIdx.x;
	yIndex = blockIdx.x * BLOCK_SIZE + threadIdx.y;
	int outIndex = yIndex * height + xIndex;

	tile_real[threadIdx.y][threadIdx.x] = matrix_in[inIndex].x;
	tile_imag[threadIdx.y][threadIdx.x] = matrix_in[inIndex].y;

	__syncthreads();

	matrix_out[outIndex].x = tile_real[threadIdx.x][threadIdx.y];
	matrix_out[outIndex].y = tile_imag[threadIdx.x][threadIdx.y];
}

extern __global__ void IIR_filter(float2* spectrum,
	int sample1,
	int sample2)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index < sample1)
	{
		spectrum[index].x = 0.0f;
		spectrum[index].y = 0.0f;
	}
	else if (index > sample2)
	{
		spectrum[index].x = 0.0f;
		spectrum[index].y = 0.0f;
	}
	else
	{
		spectrum[index].x = spectrum[index].x;
		spectrum[index].y = spectrum[index].y;
	}
}