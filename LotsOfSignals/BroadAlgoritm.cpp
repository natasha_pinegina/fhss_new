#include "pch.h"
#include "Methods.h"
#include "Generation.h"
#include "LotsOfSignalsDlg.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

vector<complex<double>> ffiltr_comp;

//vector<complex<double>> filtr{ {-0.00467840531359536,0},
//	{-0.0227307623078218,0},
//	{-0.0101367492124695,0},
//	{0.00914542211957187,0},
//	{-0.000295380385483888,0},
//	{-0.00719471736279598,0},
//	{0.00704620687365456,0},
//	{0.000895702227195519,0},
//	{-0.00861929018060198,0},
//	{0.00709883606518327,0},
//	{0.00321956762528475,0},
//	{-0.0111740804479671,0},
//	{0.00666660537639947,0},
//	{0.00683031999132339,0},
//	{-0.0141109852876007,0},
//	{0.00512441178816228,0},
//	{0.0118795784240313,0},
//	{-0.0171263012201244,0},
//	{0.00186367820092450,0},
//	{0.0187645927037202,0},
//	{-0.0199529300356029,0},
//	{-0.00400720244253652,0},
//	{0.0285063086882292,0},
//	{-0.0224045772288029,0},
//	{-0.0147083314520262,0},
//	{0.0439495329319744,0},
//	{-0.0242796240120923,0},
//	{-0.0374085435570948,0},
//	{0.0771782881951977,0},
//	{-0.0254769677958388,0},
//	{-0.122796198002549,0},
//	{0.287490021392642,0},
//	{0.640786107248223,0},
//	{0.287490021392642,0},
//	{-0.122796198002549,0},
//	{-0.0254769677958388,0},
//	{0.0771782881951977,0},
//	{-0.0374085435570948,0},
//	{-0.0242796240120923,0},
//	{0.0439495329319744,0},
//	{-0.0147083314520262,0},
//	{-0.0224045772288029,0},
//	{0.0285063086882292,0},
//	{-0.00400720244253652,0},
//	{-0.0199529300356029,0},
//	{0.0187645927037202,0},
//	{0.00186367820092450,0},
//	{-0.0171263012201244,0},
//	{0.0118795784240313,0},
//	{0.00512441178816228,0},
//	{-0.0141109852876007,0},
//	{0.00683031999132339,0},
//	{0.00666660537639947,0},
//	{-0.0111740804479671,0},
//	{0.00321956762528475,0},
//	{0.00709883606518327,0},
//	{-0.00861929018060198,0},
//	{0.000895702227195519,0},
//	{0.00704620687365456,0},
//	{-0.00719471736279598,0},
//	{-0.000295380385483888,0},
//	{0.00914542211957187,0},
//	{-0.0101367492124695,0},
//	{-0.0227307623078218,0},
//	{-0.00467840531359536,0}};


vector<complex<double>> filtr{ 
	{-0.00159725104829965,0},
	{-0.00268509017776896,0},
	{-0.00302511643507043,0},
	{-0.00135476205254851,0},
	{0.00248714252586071,0},
	{0.00719049945539460,0},
	{0.0104046669234085,0},
	{0.0101126754445063,0},
	{0.00613648572292398,0},
	{0.000606330950821287,0},
	{-0.00318355168644539,0},
	{-0.00296783048356539,0},
	{0.000766259204381997,0},
	{0.00493160838650974,0},
	{0.00599362536209646,0},
	{0.00268960676286970,0},
	{-0.00277776861633081,0},
	{-0.00620301606746652,0},
	{-0.00461153538817482,0},
	{0.00116116752844799,0},
	{0.00673342059060043,0},
	{0.00739980155913788,0},
	{0.00200014215871249,0},
	{-0.00569360588534352,0},
	{-0.00952478665250405,0},
	{-0.00579948793072773,0},
	{0.00331175226252151,0},
	{0.0108380910939918,0},
	{0.0102226751706250,0},
	{0.000874834214628389,0},
	{-0.0105245342673602,0},
	{-0.0146225114191913,0},
	{-0.00687125474428472,0},
	{0.00794581364997604,0},
	{0.0183284223794270,0},
	{0.0146850763215554,0},
	{-0.00228567624326458,0},
	{-0.0204364048220847,0},
	{-0.0243517287013341,0},
	{-0.00769715016990109,0},
	{0.0196403834462923,0},
	{0.0363498600492459,0},
	{0.0248152840762666,0},
	{-0.0133769228727413,0},
	{-0.0533752092454361,0},
	{-0.0595349221802762,0},
	{-0.00814981541669089,0},
	{0.0939849128752219,0},
	{0.208363031412261,0},
	{0.283559462899941,0},
	{0.283559462899941,0},
	{0.208363031412261,0},
	{0.0939849128752219,0},
	{-0.00814981541669089,0},
	{-0.0595349221802762,0},
	{-0.0533752092454361,0},
	{-0.0133769228727413,0},
	{0.0248152840762666,0},
	{0.0363498600492459,0},
	{0.0196403834462923,0},
	{-0.00769715016990109,0},
	{-0.0243517287013341,0},
	{-0.0204364048220847,0},
	{-0.00228567624326458,0},
	{0.0146850763215554,0},
	{0.0183284223794270,0},
	{0.00794581364997604,0},
	{-0.00687125474428472,0},
	{-0.0146225114191913,0},
	{-0.0105245342673602,0},
	{0.000874834214628389,0},
	{0.0102226751706250,0},
	{0.0108380910939918,0},
	{0.00331175226252151,0},
	{-0.00579948793072773,0},
	{-0.00952478665250405,0},
	{-0.00569360588534352,0},
	{0.00200014215871249,0},
	{0.00739980155913788,0},
	{0.00673342059060043,0},
	{0.00116116752844799,0},
	{-0.00461153538817482,0},
	{-0.00620301606746652,0},
	{-0.00277776861633081,0},
	{0.00268960676286970,0},
	{0.00599362536209646,0},
	{0.00493160838650974,0},
	{0.000766259204381997,0},
	{-0.00296783048356539,0},
	{-0.00318355168644539,0},
	{0.000606330950821287,0},
	{0.00613648572292398,0},
	{0.0101126754445063,0},
	{0.0104046669234085,0},
	{0.00719049945539460,0},
	{0.00248714252586071,0},
	{-0.00135476205254851,0},
	{-0.00302511643507043,0},
	{-0.00268509017776896,0},
	{-0.00159725104829965,0}};

vector<double> freq_sdvig{ 240e6,
						237e6,
						234e6,
						231e6,
						228e6,
						225e6,
						222e6,
						219e6,
						216e6,
						213e6,
						210e6,
						207e6,
						204e6,
						201e6,
						156e6,
						153e6,
						150e6,
						147e6,
						144e6,
						96e6,
						93e6,
						90e6,
						87e6,
						84e6,
						81e6,
						78e6,
						75e6,
						72e6,
						69e6,
						66e6,
						63e6,
						60e6,
						57e6,
						54e6,
						51e6,
						48e6,
						45e6,
						42e6,
						39e6,
						36e6,
						33e6,
						30e6,
						27e6,
						24e6,
						21e6,
						18e6,
						15e6,
						12e6,
						9e6,
						6e6};

using namespace std;


void CLotsOfSignalsDlg::ReadDataFromFile(vector<double>& koef, char* way)
{
	string line;
	ifstream in(way);
	if (in.is_open())
	{
		while (getline(in, line))
		{
			koef.push_back(atof(line.c_str()));
		}
	}
}

//void CLotsOfSignalsDlg::ConvolutionHS(Signal& signal, Signal& convolution)
//{
//	__int64 timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
//	convolution.signal.clear();
//	convolution.keys.clear();
//	convolution.sampling = signal.sampling;
//	convolution.timestamp = 0;
//	//convolution.duration = signal1.duration + signal2.duration;
//	for (unsigned int n = 0; n <signal.signal.size(); n++)
//	{
//		complex<double> counter = complex<double>(0, 0);
//		for (unsigned int m = 0; m < filtr.size(); m++)
//		{
//			//complex<double> h = { filter[m], 0 };
//			int iter = n - m;
//			if (iter < 0)
//				//counter += signal.signal[signal.signal.size()+(iter)] * filtr[m];
//				continue;
//			else 
//				counter += signal.signal[iter] * filtr[m];
//		}
//		convolution.signal.push_back(counter);
//		convolution.keys.push_back(n);
//	}
//}

void CLotsOfSignalsDlg::ConvolutionHS(Signal& signal, Signal& convolution)
{
	__int64 timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	convolution.signal.clear();
	convolution.keys.clear();
	convolution.sampling = signal.sampling;
	convolution.timestamp = 0;
	int t = 0;
	for (int n = 0; n < (int)(signal.signal.size()); n++)
	{
		complex<double> counter = complex<double>(0, 0);
		for (int m = 0; m < ffiltr_comp.size(); m++)
		{
			if ((-m + n) >= 0)
				counter += signal.signal[-m + n] * ffiltr_comp[m];
			else
				continue;
		}
		convolution.signal.push_back(counter);
		convolution.keys.push_back(t);
		t++;
	}
}
void CLotsOfSignalsDlg::ExponentMultiplication(Signal& signal, double sdvig, double direction)
{
	auto comp_j = complex<double>(0, 1);
	for (int i = 0; i < signal.signal.size(); i++)
	{
		signal.signal[i] = signal.signal[i] * exp(comp_j*direction * 2.0 * M_PI * sdvig * signal.keys[i]);
	}
};

void CLotsOfSignalsDlg::ExtractionOfNarrowbandSignal(vector<Signal>& signal,vector<vector<Signal>>& conv)
{

	vector<double> koef;
	ReadDataFromFile(koef, "ffiltr.txt");
	ffiltr_comp.clear();
	ffiltr_comp.resize(koef.size());
	for (int i = 0; i < koef.size(); i++)
	{
		complex<double> C{ koef[i],0 };
		ffiltr_comp[i] = C;
	}
	for (int n = 0; n < signal.size(); n++)
	{
		conv[n].resize(50);
		int shag = 0;
		for (int i = 0; i < 50; i++)
		{
			ExponentMultiplication(signal[n], freq_sdvig[i], 1);
			ConvolutionHS(signal[n], conv[n][i]);
			ExponentMultiplication(signal[n], freq_sdvig[i], -1);
		}
	}
}

void CLotsOfSignalsDlg::SummA(vector<Signal2D>& A, Signal2D& newA)
{
	//newA.resize(A.size());
	
		newA.keysX.resize(A[0].keysX.size());
		newA.keysY.resize(A[0].keysY.size());
		newA.sampling = A[0].sampling;
		newA.signal.resize(A[0].signal.size());

		for (int j = 0; j < A[0].signal.size(); j++)
			newA.signal[j].resize(A[0].signal[j].size());
	
		newA.keysX = A[0].keysX;
		newA.keysY = A[0].keysY;
	
		for (int i = 0; i < A.size(); i++)
		{
			for (int j = 0; j < A[i].keysY.size(); j++)
			{
				for (int k = 0; k < A[i].keysX.size(); k++)
				{
					newA.signal[j][k] = newA.signal[j][k] + A[i].signal[j][k];
				}
			}
		}
}